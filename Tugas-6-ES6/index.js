// Soal 1
function luasPersegi(panjang, tinggi){
    return panjang * tinggi;
}

function kelilingPersegi(panjang, tinggi){
    return (2*panjang) + (2*tinggi);
}

let panjang = 5;
let lebar = 2;
console.log(luasPersegi(panjang, lebar));
console.log(kelilingPersegi(panjang, lebar));

// Soal 2
const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}
   
// Driver Code 
newFunction("William", "Imoh").fullName() 

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Rancamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

//Driver Code
const combinedWestEast = [...west, ...east]
console.log(combinedWestEast)

// Soal 5
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)
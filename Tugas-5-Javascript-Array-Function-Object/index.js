// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log(daftarHewan);
console.log('\n');

// Soal 2
function introduce(biodata){
    return biodata = ("Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby);
}
var data = {name : "John", age : 30, address : "Jalan Pelesiran", hobby : "Gaming"}
var perkenalan = introduce(data)
console.log(perkenalan);
console.log('\n');

// Soal 3
function hitung_huruf_vokal(hurufVokal){
    return hurufVokal.match(/[aiueo]/gi).length;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2);
console.log('\n');

// Soal 4
function hitung(angka){
    return angka = -2 + angka + angka;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));

// Quiz
function next_date(){
    //var hariIni = new Date(newTahun, newBulan, newTanggal);
    //besok.setDate(besok.getDate()+1);
    //newTanggal = besok.getDate()+1;
    var besok = new Date();
    besok.setDate(besok.getDate()+1);



    switch(newBulan){
        case 1: {console.log(newTanggal + ' Januari ' + newTahun); break;}
        case 2: {console.log(newTanggal + ' Februari ' + newTahun); break;}
        case 3: {console.log(newTanggal + ' Maret ' + newTahun); break;}
        case 4: {console.log(newTanggal + ' April ' + newTahun); break;}
        case 5: {console.log(newTanggal + ' Mei ' + newTahun); break;}
        case 6: {console.log(newTanggal + ' Juni ' + newTahun); break;}
        case 7: {console.log(newTanggal + ' Juli ' + newTahun); break;}
        case 8: {console.log(newTanggal + ' Agustus ' + newTahun); break;}
        case 9: {console.log(newTanggal + ' September ' + newTahun); break;}
        case 10: {console.log(newTanggal + ' Oktober ' + newTahun); break;}
        case 11: {console.log(newTanggal + ' November ' + newTahun); break;}
        case 12: {console.log(newTanggal + ' Desember ' + newTahun); break;}
        default: {console.log('Tidak terjadi apa-apa');}
    }

    return besok;   
}
var newTanggal = 7;
var newBulan = 2;
var newTahun = 2020;
console.log(next_date(newTanggal, newBulan, newTahun));
var hariEsok = new Date();
hariEsok.setDate(hariEsok.getDate()+1);
console.log(hariEsok);

function jumlah_kata(kalimat){
    return kalimat.split(" ").length;
}
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));
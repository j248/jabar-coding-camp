var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Soal 2
var waktu = 10000;

readBooksPromise(waktu, books[0])
.then(sisaWaktu0 => {
    readBooksPromise(sisaWaktu0, books[1])
    .then(sisaWaktu1 => {
        readBooksPromise(sisaWaktu1, books[2])
        .then(sisaWaktu2 => {
            readBooksPromise(sisaWaktu2, books[3])
            .then(sisaWaktu3 => {
                return
            })
        })
    })
})
.catch(error => console.log(error))
// Soal 1
var nilai=79;

if(nilai>=85){
    console.log("Indeks A");
}else if(nilai>=75){
    console.log("Indeks B");
}else if(nilai>=65){
    console.log("Indeks C");
}else if(nilai>=55){
    console.log("Indeks D");
}else {
    console.log("Indeks E");
}
console.log('\n');

// Soal 2
var tanggal=14;
var bulan=5;
var tahun=1993;

switch(bulan){
    case 1: {console.log(tanggal + ' Januari ' + tahun); break;}
    case 2: {console.log(tanggal + ' Februari ' + tahun); break;}
    case 3: {console.log(tanggal + ' Maret ' + tahun); break;}
    case 4: {console.log(tanggal + ' April ' + tahun); break;}
    case 5: {console.log(tanggal + ' Mei ' + tahun); break;}
    case 6: {console.log(tanggal + ' Juni ' + tahun); break;}
    case 7: {console.log(tanggal + ' Juli ' + tahun); break;}
    case 8: {console.log(tanggal + ' Agustus ' + tahun); break;}
    case 9: {console.log(tanggal + ' September ' + tahun); break;}
    case 10: {console.log(tanggal + ' Oktober ' + tahun); break;}
    case 11: {console.log(tanggal + ' November ' + tahun); break;}
    case 12: {console.log(tanggal + ' Desember ' + tahun); break;}
    default: {console.log('Tidak terjadi apa-apa');}
}
console.log('\n');

// Soal 3
var n=7;

for(var tinggi=1; tinggi<=n; tinggi++){
    var x='';
    for(var alas=1; alas<=tinggi; alas++){
        var x = x + "#";
    }
    console.log(x);
}

// Soal 4
var m=4;
const teks=["I love programming", "I love Javascript", "I love Vue Js"];
let i = 0;
let teksID="";

for(var deret=1; deret<=m; deret++){
    for(let i=0; i<teks.length; i++){
        teksID = teks[i];
    }
    console.log(deret + ' - ' + teksID);
}
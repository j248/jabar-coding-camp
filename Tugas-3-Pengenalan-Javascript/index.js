// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// Jawaban
var newPertama1 = pertama.substr(0, 5);
var newPertama2 = pertama.substr(12, 7);
var newKedua1 = kedua.substr(0, 8);
var newKedua2 = kedua.substr(8, 10);
console.log(newPertama1.concat(newPertama2).concat(newKedua1).concat(newKedua2.toUpperCase()));

// Soal 2
var kataPertama = Number("10");
var kataKedua =  Number("2");
var kataKetiga =  Number("4");
var kataKeempat =  Number("6");

// Jawaban
console.log((kataPertama+kataKeempat-kataKetiga)*kataKedua);

// Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);